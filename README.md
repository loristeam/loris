# Loris 

To start the project, you need to download the database from the second repository(graphDb),<br />
and put in the var folder.  
  
The file structure should look like:   
  
  
   
+ src/ 
     * java/ 
     * resources/  
- var/      
     - graphDb/
     - logs/
	 - store_lock
- .gitignore
- ...
