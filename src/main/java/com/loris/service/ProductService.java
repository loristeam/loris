package com.loris.service;

import com.loris.database.GraphDatabaseManager;
import com.loris.model.Product;
import com.loris.model.exceptions.ExceptionFactory;
import com.loris.model.exceptions.ProductNotFoundException;
import com.loris.util.ProductTypeManager;
import org.neo4j.graphdb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
 * TODO: Make service as instance and implemet methods here. Make the same with others services.
 */

@Service
public class ProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);
    @Autowired
    private final GraphDatabaseManager graphManager;
    @Autowired
    private final ProductTypeManager productTypeManager;
    private static final Label PRODUCT_LABEL = Label.label(Product.LABEL);
    private static final Label PRODUCT_ROOT_LABEL = Label.label("PRODUCT_ROOT");
    private static final RelationshipType HAS_PRODUCT_GROUP = RelationshipType.withName("has_product_group");
    private static final RelationshipType IS_TYPE = RelationshipType.withName("is_type");
    private static final String PRODUCT_NEXT_ID = "nextId";
    private static final String PRODUCT_NOT_FOUND_MSG = "Product not found.";


    public ProductService(GraphDatabaseManager graphManager, ProductTypeManager productTypeManager) {
        this.graphManager = graphManager;
        this.productTypeManager = productTypeManager;
    }

    public void addProduct(Product p) {
        //TODO: Check valid of product and check if it exist in database. Throws exception when something wrong.
        if(productTypeManager.getPrefix(p.getType())==null){
            p.setType("OTHER");
        }

        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node productNode = graphManager.getGraphDb().createNode(PRODUCT_LABEL);
            Node productGroupNode = getProductGroupNode(p.getType());
            int nextId = (int) productGroupNode.getProperty(PRODUCT_NEXT_ID);
            String id = generateId(nextId,p.getType());
            p.setId(id);
            setProperties(productNode, p);
            productGroupNode.setProperty(PRODUCT_NEXT_ID, ++nextId);
            graphManager.createRelationship(productNode,IS_TYPE,productGroupNode);
            tx.success();
        }
    }

    private String generateId(long number, String type) {
        String prefix = productTypeManager.getPrefix(type);
        return String.format("%s%s", prefix, number);
    }

    public Set getAllProducts() {
        Set<Product> result = new HashSet<>();
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            ResourceIterator<Node> nodes = graphManager.getGraphDb().findNodes(PRODUCT_LABEL);
            for (ResourceIterator<Node> it = nodes; it.hasNext(); ) {
                Node n = it.next();
                Product p = extractProductFromNode(n);
                result.add(p);
            }
            tx.success();
        }
        return result;
    }

    private void setProperties(Node node, Product p) {
        node.setProperty(Product.NAME, p.getName());
        node.setProperty(Product.CALORIES, p.getCal());
        node.setProperty(Product.TYPE, p.getType());
        node.setProperty(Product.ID, p.getId());
    }

    private Product extractProductFromNode(Node node) throws NotFoundException {
        Product result = new Product();
        result.setName(node.getProperty(Product.NAME).toString());
        String type = (String) node.getProperty(Product.TYPE);
        result.setType(type);
        int calories = (int) node.getProperty(Product.CALORIES);
        result.setCal(calories);
        String id = (String) node.getProperty(Product.ID);
        result.setId(id);
        return result;
    }

    private Node getProductGroupNode(String group) {
        Node groupNode = null;
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            ResourceIterator<Node> nodesByLabel = graphManager.getNodesByLabel(Label.label(group));
            for (ResourceIterator<Node> it = nodesByLabel; it.hasNext(); ) {
                groupNode = it.next();
                break;
            }
            if (groupNode == null) {
                groupNode = createGroupNode(group);
                groupNode.setProperty("name", group);
            }
            tx.success();
        }
        return groupNode;
    }

    private Node createGroupNode(String group) {
        Node groupNode = graphManager.getGraphDb().createNode(Label.label(group));
        Node rootNode = null;
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            groupNode.setProperty(PRODUCT_NEXT_ID, 0);
            ResourceIterator<Node> nodesByLabel = graphManager.getNodesByLabel(PRODUCT_ROOT_LABEL);

            for (ResourceIterator<Node> it = nodesByLabel; it.hasNext(); ) {
                rootNode = it.next();
                break;
            }
            if (rootNode == null) {
                rootNode = graphManager.createRootNode(PRODUCT_ROOT_LABEL);
                rootNode.setProperty("name", "Product Root");
            }

            graphManager.createRelationship(rootNode, HAS_PRODUCT_GROUP, groupNode);

            tx.success();
        }

        return groupNode;
    }

    public void updateProduct(Product product) throws ProductNotFoundException {
        //TODO:Some validations/checking nodes, when product not exist throw exception.
        try(Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node node = graphManager.getGraphDb().findNode(PRODUCT_LABEL, Product.ID, product.getId());
            if(!isProductNode(node)){
                throw ExceptionFactory.createProductNotFound(PRODUCT_NOT_FOUND_MSG);
            }
            Product p = extractProductFromNode(node);
            LOGGER.debug(p.toString());
            setProperties(node,product);
            LOGGER.debug(product.toString());
            tx.success();
        }

    }

    private boolean isProductNode(Node node){
        if(node==null){
            return false;
        }
        Iterable<Label> labels = node.getLabels();
        for (Iterator<Label> iterator = labels.iterator(); iterator.hasNext(); ) {
            Label n = iterator.next();
            if (n.equals(PRODUCT_LABEL)){
                return true;
            }
        }
        return false;
    }

    public void deleteProduct(Product product) {
       //not supported yet
    }

    public Product getProduct(String id) throws ProductNotFoundException {
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node productNode = graphManager.getGraphDb().findNode(PRODUCT_LABEL,Product.ID, id);
            if(productNode == null){
                 throw ExceptionFactory.createProductNotFound(PRODUCT_NOT_FOUND_MSG);
            }
            Product resProduct = extractProductFromNode(productNode);
            tx.success();
            return resProduct;
        }
    }
}
