package com.loris.service;

import com.google.gson.Gson;
import com.loris.database.GraphDatabaseManager;
import com.loris.model.IngredientsDetails;
import com.loris.model.Product;
import com.loris.model.Recipe;
import com.loris.model.exceptions.ExceptionFactory;
import com.loris.model.exceptions.IncorrectRecipeId;
import com.loris.model.exceptions.ProductNotFoundException;
import com.loris.model.exceptions.RecipeNotFoundException;
import org.neo4j.graphdb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class RecipeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeService.class);
    private static final Label RECIPE_LABEL = Label.label(Recipe.LABEL);
    private static final String RECIPE_GROUP = "RECIPE_GROUP";
    private static final Label RECIPE_GROUP_LABEL = Label.label(RECIPE_GROUP);
    private static final String RECIPE_NEXT_ID = "recipe_id";
    private static final String RECIPE_PREFIX = "r_";
    private static final String RECIPE_ID_EXCEPTION = "Recipe id is not the same. You can't change recipe id.";
    private static final String RECIPE_NOT_FOUND_EXCEPTION = "Recipe not found for given id = %s";

    @Autowired
    private final GraphDatabaseManager graphManager;
    @Autowired
    private final ProductService productService;

    public RecipeService(GraphDatabaseManager graphManager, ProductService productService) {
        this.graphManager = graphManager;
        this.productService = productService;
    }

    public void addRecipe(Recipe recipe) throws ProductNotFoundException {
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node recipeNode = graphManager.getGraphDb().createNode(RECIPE_LABEL);
            Node recipeGroupNode = getRecipeGroupNode();
            int nextId = Integer.parseInt(recipeGroupNode.getProperty(RECIPE_NEXT_ID).toString());
            String id = generateId(nextId);
            for (IngredientsDetails details:
                 recipe.getIngredients()) {
                if(details.getProductName()==null){
                    Product product = productService.getProduct(details.getProductId());
                    details.setProductName(product.getName());
                }
            }
            recipe.setId(id);
            setProperties(recipeNode, recipe);
            recipeGroupNode.setProperty(RECIPE_NEXT_ID, ++nextId);
            associateProducts(recipeNode, recipe.getIngredients());
            tx.success();
        }
    }

    public void editRecipe(String id, Recipe r) throws IncorrectRecipeId, RecipeNotFoundException {
        if (!id.equals(r.getId())) {
            throw new IncorrectRecipeId(RECIPE_ID_EXCEPTION);
        }
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node node = graphManager.getGraphDb().findNode(RECIPE_LABEL, Recipe.ID, id);
            if (node == null) {
                String msg = String.format(RECIPE_NOT_FOUND_EXCEPTION, id);
                throw ExceptionFactory.createRecipeNotFound(msg);
            }
            setProperties(node, r);
            tx.success();
        }
    }

    public Recipe getRecipe(String id) throws RecipeNotFoundException {
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            Node node = graphManager.getGraphDb().findNode(RECIPE_LABEL, Recipe.ID, id);
            if (node == null) {
                String msg = String.format(RECIPE_NOT_FOUND_EXCEPTION, id);
               throw ExceptionFactory.createRecipeNotFound(msg);
            }
            Recipe result = extractRecipeFromNode(node);
            tx.success();

            return result;
        }
    }

    public void deleteRecipe(String id) {
        //not supported yet
    }

    public List<Recipe> getAllRecipes() {
        List<Recipe> result = new LinkedList<>();
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {
            ResourceIterator<Node> nodes = graphManager.getGraphDb().findNodes(RECIPE_LABEL);
            for (ResourceIterator<Node> it = nodes; it.hasNext(); ) {
                Node n = it.next();
                Recipe recipe = extractRecipeFromNode(n);
                result.add(recipe);
            }
            tx.success();
        }
        return result;
    }

    public List<Recipe> getRandomRecipes(int amount) {
        List<Recipe> allRecipes = getAllRecipes();
        Collections.shuffle(allRecipes);
        return allRecipes.subList(0,amount<allRecipes.size()?amount:allRecipes.size());
    }

    private void associateProducts(Node recipeNode, List<IngredientsDetails> ingredients) {
        GraphDatabaseService graphDb = graphManager.getGraphDb();
        for (IngredientsDetails ingredient : ingredients) {
            Node productNode = graphDb.findNode(Label.label(Product.LABEL), Product.ID, ingredient.getProductId());
            //TODO:throws exception when product not found or not
            if (productNode != null) {
                graphManager.createRelationship(recipeNode, Recipe.USE_PRODUCT, productNode);
            }
        }
    }

    //TODO: Change storage structures
    private void setProperties(Node node, Recipe recipe) {
        Gson gson = new Gson();
        node.setProperty(Recipe.ID, recipe.getId());
        String releaseDate = gson.toJson(recipe.getRelease());
        node.setProperty(Recipe.RELEASE_DATE, releaseDate);

        node.setProperty(Recipe.TITLE, recipe.getTitle());
        String ingredients = gson.toJson(recipe.getIngredients());
        node.setProperty(Recipe.INGREDIENTS, ingredients);

        node.setProperty(Recipe.DESCRIPTION, recipe.getDescription());
        node.setProperty(Recipe.STEPS, recipe.getSteps());
        node.setProperty(Recipe.IMG_URL, recipe.getImgUrl());
    }

    public Recipe extractRecipeFromNode(Node node) {
        Gson gson = new Gson();
        Recipe result = new Recipe();
        result.setId(node.getProperty(Recipe.ID).toString());

        LocalDateTime release = gson.fromJson(node.getProperty(Recipe.RELEASE_DATE).toString(), LocalDateTime.class);
        result.setRelease(release);

        List<IngredientsDetails> ingredients = gson.fromJson(node.getProperty(Recipe.INGREDIENTS).toString(), Recipe.INGREDIENTS_TYPE);
        result.setIngredients(ingredients);

        result.setTitle(node.getProperty(Recipe.TITLE).toString());
        result.setDescription(node.getProperty(Recipe.DESCRIPTION).toString());
        result.setSteps(node.getProperty(Recipe.STEPS).toString());
        result.setImgUrl(node.getProperty(Recipe.IMG_URL).toString());

        return result;
    }

    private Node getRecipeGroupNode() {
        Node groupNode = null;
        ResourceIterator<Node> nodesByLabel = graphManager.getNodesByLabel(RECIPE_GROUP_LABEL);
        for (ResourceIterator<Node> it = nodesByLabel; it.hasNext(); ) {
            groupNode = it.next();
            break;
        }
        if (groupNode == null) {
            groupNode = createGroupNode();
            groupNode.setProperty("name", RECIPE_GROUP);
            groupNode.setProperty(RECIPE_NEXT_ID, 0);
        }
        return groupNode;
    }

    private Node createGroupNode() {
        return graphManager.getGraphDb().createNode(RECIPE_GROUP_LABEL);
    }

    private String generateId(int number) {
        return String.format("%s%s", RECIPE_PREFIX, number);
    }
}
