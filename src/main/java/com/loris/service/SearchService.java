package com.loris.service;

import com.loris.database.GraphDatabaseManager;
import com.loris.model.IngredientsDetails;
import com.loris.model.Product;
import com.loris.model.Recipe;
import com.loris.model.SearchResult;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SearchService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchService.class);
    @Inject
    private final GraphDatabaseManager graphManager;
    private final RecipeService recipeService;

    public SearchService(GraphDatabaseManager graphManager, RecipeService recipeService) {
        this.graphManager = graphManager;
        this.recipeService = recipeService;
    }

    public List<SearchResult> search(List<String> listProducts) {
        try (Transaction tx = graphManager.getGraphDb().beginTx()) {

            Set<Recipe> tempSet = getRecipesUsingProducts(listProducts);
            List<SearchResult> result = calculateRelevancy(listProducts, tempSet);
            result.sort(Comparator.comparingDouble(SearchResult::getRelevancy).reversed());
            tx.success();
            return result;
        }
    }

    private List<SearchResult> calculateRelevancy(List<String> listProducts, Set<Recipe> tempSet) {
        List<SearchResult> results = new LinkedList<>();
        tempSet.forEach(recipe -> {
            List<IngredientsDetails> ingredients = recipe.getIngredients();
            double relevancy = (double)countCommonElements(ingredients,listProducts)/ingredients.size();
            SearchResult searchResult = new SearchResult();
            searchResult.setRecipe(recipe);
            searchResult.setRelevancy(relevancy);
            results.add(searchResult);
        });

        return results;
    }

    private int countCommonElements(List<IngredientsDetails> ingredients, List<String> listProducts) {
        AtomicInteger count = new AtomicInteger();
        ingredients.forEach(ingredient -> {if(listProducts.contains(ingredient.getProductId())) count.getAndIncrement();});
        return count.get();
    }

    private Set<Recipe> getRecipesUsingProducts(List<String> listProducts) {
        Set<Recipe> result = new HashSet<>();

        for (String productId : listProducts) {
            Node node = graphManager.getGraphDb().findNode(Label.label(Product.LABEL), Product.ID, productId);
            if(node==null){
                //TODO: not found, throw exception
                continue;
            }
            Iterable<Relationship> relationships = node.getRelationships(Recipe.USE_PRODUCT);
            relationships.forEach(relationship -> {
                Node startNode = relationship.getStartNode();
                result.add(recipeService.extractRecipeFromNode(startNode));
            });
        }
        return result;
    }
}
