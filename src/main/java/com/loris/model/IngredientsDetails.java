package com.loris.model;

import java.util.Objects;

public class IngredientsDetails {
    private String productId;
    private String productName;
    private String amount;

    public IngredientsDetails() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IngredientsDetails that = (IngredientsDetails) o;
        return Objects.equals(productId, that.productId) &&
                Objects.equals(productName, that.productName) &&
                Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productName, amount);
    }

    @Override
    public String toString() {
        return "IngredientsDetails{" +
                "productId='" + productId + '\'' +
                ", productName='" + productName + '\'' +
                ", amount='" + amount + '\'' +
                '}';
    }
}
