package com.loris.model.exceptions;

public class IncorrectRecipeId extends Exception {
    public IncorrectRecipeId(String message) {
        super(message);
    }
}
