package com.loris.model.exceptions;


public class ExceptionFactory {
    
    public static IncorrectRecipeId createIncorectRecipeId(String text){
        return new IncorrectRecipeId(text);
    }
    public static ProductNotFoundException createProductNotFound(String text){
        return new ProductNotFoundException(text);
    }
    public static RecipeNotFoundException createRecipeNotFound(String text){
        return new RecipeNotFoundException(text);
    }

}
