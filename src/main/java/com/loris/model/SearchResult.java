package com.loris.model;

public class SearchResult {

    private double relevancy;
    private Recipe recipe;

    public SearchResult() {
    }

    public double getRelevancy() {
        return relevancy;
    }

    public void setRelevancy(double relevancy) {
        this.relevancy = relevancy;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
}
