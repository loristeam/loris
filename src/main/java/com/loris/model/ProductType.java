package com.loris.model;

import java.util.Map;

public interface ProductType {
    String getPrefix(String name);
    Map<String, String> getTypesMap();
}
