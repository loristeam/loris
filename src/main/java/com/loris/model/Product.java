package com.loris.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

    public static final String LABEL = "PRODUCT";
    public static final String NAME = "name";
    public static final String CALORIES = "calories";
    public static final String TYPE = "type";
    public static final String ID = "id";


    private String id;
    private String name;
    @JsonProperty("calories")
    private int cal;
    private String type;


    public Product() {
    }

    public Product(String name, int cal, String type) {
        this.name = name;
        this.cal = cal;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCal() {
        return cal;
    }

    public void setCal(int cal) {
        this.cal = cal;
    }

    public String getType() {return type;}

    //TODO: check validity of type using enum Type, if good set value, if others throw exception.
    public void setType(String type) {this.type = type;}

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", cal=" + cal +
                ", type='" + type + '\'' +
                '}';
    }
}
