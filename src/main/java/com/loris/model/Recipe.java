package com.loris.model;

import com.google.gson.reflect.TypeToken;
import org.neo4j.graphdb.RelationshipType;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

public class Recipe {
    public static final Type INGREDIENTS_TYPE  = new TypeToken<List<IngredientsDetails>>(){}.getType();
    public static final RelationshipType USE_PRODUCT = RelationshipType.withName("use_product");

    public static final String LABEL = "RECIPE";
    public static final String ID = "id";
    public static final String RELEASE_DATE = "release";
    public static final String TITLE = "title";
    public static final String INGREDIENTS = "ingredients";
    public static final String DESCRIPTION = "description";
    public static final String STEPS = "steps";
    public static final String IMG_URL = "img-url";

    private String id;
    private LocalDateTime release;
    private String title;

    //TODO: change map to most comfortable struct to use and checking in future
    //Map <productID, productQuantity>
    private List<IngredientsDetails> ingredients;

    private String description;
    private String steps;
    private String imgUrl;

    public Recipe() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getRelease() {
        return release;
    }

    public void setRelease(LocalDateTime release) {
        this.release = release;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<IngredientsDetails> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<IngredientsDetails> ingredients) {
        this.ingredients = ingredients;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id='" + id + '\'' +
                ", release=" + release +
                ", title='" + title + '\'' +
                ", ingredients=" + ingredients +
                ", description='" + description + '\'' +
                ", steps='" + steps + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return Objects.equals(id, recipe.id) &&
                Objects.equals(release, recipe.release) &&
                Objects.equals(title, recipe.title) &&
                Objects.equals(ingredients, recipe.ingredients) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(steps, recipe.steps) &&
                Objects.equals(imgUrl, recipe.imgUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, release, title, ingredients, description, steps, imgUrl);
    }
}
