package com.loris.controller;

import com.loris.Application;
import com.loris.model.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class StatusController {
    @Autowired
    private Environment env;

    private static final Logger LOGGER = LoggerFactory.getLogger(StatusController.class);

    @GetMapping("/status")
    public ResponseEntity getStatus() {
        LOGGER.debug("[GET]:status");
        Status status = new Status(env.getProperty(Application.CONFIG_NAME),
                "running",
                env.getProperty(Application.CONFIG_VERSION));
        return ResponseEntity.ok().body(status);
    }
}
