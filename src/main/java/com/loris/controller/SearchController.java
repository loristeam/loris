package com.loris.controller;

import com.loris.model.SearchResult;
import com.loris.service.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/search")
public class SearchController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    private final SearchService searchService;
    private static final String PRODUCTS_IS_REQUIRED = "Products is required to search.";

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping
    public ResponseEntity search(@RequestBody List<String> products) {
        LOGGER.debug("[GET]:search {{}}", products);
        if(products.size()==0){
            return ResponseEntity.badRequest().body(PRODUCTS_IS_REQUIRED);
        }
        List<SearchResult> searchResults = searchService.search(products);

        return ResponseEntity.ok().body(searchResults);
    }
}
