package com.loris.controller;

import com.loris.model.Recipe;
import com.loris.model.exceptions.ProductNotFoundException;
import com.loris.model.exceptions.RecipeNotFoundException;
import com.loris.service.RecipeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/recipe")
public class RecipeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RecipeController.class);
    @Autowired
    private final RecipeService recipeService;

    public RecipeController(RecipeService rs) {
        this.recipeService = rs;
    }

    @PostMapping
    public ResponseEntity addRecipe(@RequestBody Recipe recipe) {
        LOGGER.debug("[POST]:recipe {{}}", recipe.toString());
        recipe.setRelease(LocalDateTime.now());
        try {
            recipeService.addRecipe(recipe);
        } catch (ProductNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(recipe);
    }

    @GetMapping("{id}")
    public ResponseEntity getRecipe(@PathVariable("id") String id) {
        LOGGER.debug("[GET]:recipe {{}}", id);
        Recipe recipe = null;
        try {
            recipe = recipeService.getRecipe(id);
        } catch (RecipeNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(recipe);
    }

    @GetMapping()
    public ResponseEntity getAllRecipes() {
        LOGGER.debug("[GET]:recipe");

        List<Recipe> allRecipes = recipeService.getAllRecipes();

        return ResponseEntity.ok().body(allRecipes);
    }

    @GetMapping("random/{amount}")
    public ResponseEntity getRandomRecipes(@PathVariable("amount") int amount) {
        LOGGER.debug("[GET]:recipe/random/{{}}", amount);

        List<Recipe> recipes = recipeService.getRandomRecipes(amount);

        return ResponseEntity.ok().body(recipes);
    }

}
