package com.loris.controller;

import com.loris.model.Product;
import com.loris.model.exceptions.ProductNotFoundException;
import com.loris.util.ProductTypeManager;
import com.loris.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/product")
public class ProductController {
    private final ProductService productService;
    private final ProductTypeManager productTypeManager;
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    public ProductController(ProductService productService, ProductTypeManager productTypeManager) {
        this.productService = productService;
        this.productTypeManager = productTypeManager;
    }

    @PostMapping
    public ResponseEntity addProduct(@RequestBody Product product) {
        LOGGER.debug("[POST]:product {{}}", product.toString());
        productService.addProduct(product);
        return ResponseEntity.ok().body(product);
    }

    @GetMapping
    public ResponseEntity getProducts() {
        LOGGER.debug("[GET]:product");
        Set allProducts = productService.getAllProducts();
        return ResponseEntity.ok().body(allProducts);
    }

    @GetMapping("{id}")
    public ResponseEntity getProduct(@PathVariable("id") String id) {
        LOGGER.debug("[GET]:product/ {{}}", id);
        Product product = null;
        try {
            product = productService.getProduct(id);
        } catch (ProductNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().body(product);
    }

    @GetMapping("/type")
    public ResponseEntity getProductTypes() {
        LOGGER.debug("[GET]:product/type");
        return ResponseEntity.ok().body(productTypeManager.getTypesMap());
    }

    @PutMapping
    public ResponseEntity updateProduct(@RequestBody Product product) {
        try {
            productService.updateProduct(product);
        } catch (ProductNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping
    public ResponseEntity deleteProduct(@RequestBody Product product) {
        productService.deleteProduct(product);
        return ResponseEntity.ok().build();
    }

}
