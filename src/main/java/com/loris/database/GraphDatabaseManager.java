package com.loris.database;

import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;


/* this can be use repository, but i cant implemet that to embeded database :/ */
@Component
public class GraphDatabaseManager implements AutoCloseable, ApplicationRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(GraphDatabaseManager.class);
    private GraphDatabaseService graphDb;
    private static final String FIND_NODE_BY_ID_PATTERN = "MATCH (n) WHERE n.id = '%s' RETURN n";
    private static final String DELETE_NODE_BY_ID_PATTERN = "MATCH (n) WHERE n.id = '%s' DETACH DELETE n";

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOGGER.info("Graph database service started now.");
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( new File(
                "var/graphDb") );
        registerShutdownHook(graphDb);

    }

    @Override
    public void close() throws Exception {
        LOGGER.info("Graph database service closed now.");
        graphDb.shutdown();
    }

    private static void registerShutdownHook(GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread(()->{
                LOGGER.info("Graph database service closed now.");
                graphDb.shutdown();
        }));
    }

    public GraphDatabaseService getGraphDb() {
        return graphDb;
    }

    public void createRelationship(Node left, RelationshipType relationship, Node right){
        graphDb.getNodeById(left.getId()).createRelationshipTo(right,relationship);
    }

    public ResourceIterator<Node> getNodesByLabel(Label label) {
        return graphDb.findNodes(label);
    }

    public Node createRootNode(Label label) {
        return  graphDb.createNode(label);
    }
}