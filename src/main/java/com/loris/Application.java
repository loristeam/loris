package com.loris;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static final String CONFIG_NAME = "loris.name";
    public static final String CONFIG_VERSION = "loris.version";
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
