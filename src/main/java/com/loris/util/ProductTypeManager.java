package com.loris.util;

import com.loris.model.ProductType;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ProductTypeManager implements ApplicationRunner, ProductType {

    private static final Map<String, String> types = new HashMap<>();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        types.put("FRUIT", "FR_");
        types.put("MEAT", "ME_");
        types.put("CHICKEN", "CH_");
        types.put("VEGETABLES", "VEG_");
        types.put("SEAFOOD", "SF_");
        types.put("MILK_PRODUCT", "MP_");
        types.put("NUTS_GRAINS_BEANS", "NGB_");
        types.put("EGG_PRODUCT", "EGG_");
        types.put("JUICES", "JU_");
        types.put("OTHER", "O_");
    }

    @Override
    public String getPrefix(String name) {
        for (String key: types.keySet()) {
            if(name.equals(key)){
                return types.get(key);
            }
        }
        return null;
    }

    @Override
    public Map<String, String> getTypesMap() {
        return types;
    }

}

